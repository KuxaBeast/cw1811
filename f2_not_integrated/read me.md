# read me

## main modules
### GUI.py 
This module contains all the GUI aspects and some database functions
To integrate the database to the project you will have manually specify the file path of the database and the table name
To display the images a second table has been used, the second table contains all the filepaths to the images.

to run use: ```python3 -m GUI.py```



### UTIL.py
this module contains all the functions that read and write content from the database.

## Artifact modules
### toys class
creates toys object but found no use other than when saving data into the database which was for testing purposes

### PassInfoToDatabase
this is used to write stuff into the database, was used to test the GUI.py module

# Final message
could not use frame base as the back bone made by @kuxabeast used would not work, so used a window base.
 

