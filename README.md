# All About Toys - Sales System
University of Greenwich, COMP1811 Coursework

## About
This is a group coursework project for COMP1811 - Paradigms of Programming module at the University of Greenwich.

It's built using Python 3 and Tkinter as a visual frontend.

## Usage
Application is executed by calling it as a python module:

`python3 -m toyshop`

Before running it's required to initialize the sqlite3 database using the `init_db.py` script:

`python3 init_db.py drop tables data`

For seamless operation of F2 it's also required to install `Pillow` from Pipy:

`python3 -m pip install Pillow`

or using a system package manager (eg. `pacman -S python-pillow`).

## Development
The main part of this project is being built as a python package.
Its current structure can be easily split into following parts:
- `toyshop/` - Base package
    - `__main__.py` - Called by default, initializes and runs GUI
    - `gui/` - Subpackage for GUI 
        - `core.py` - Contains base window implementation, with window frame-swapping logic
        - Contains directories with models loaded by the main window
        - Modules are imported in the `__init__.py` subpackage module
    - `controllers/` - Subpackage for controllers and data-manipulating classes
    - `models/` - Contains modules with data models which could be used as interfaces with database or just as simple data skeletons
    - `util.py` - Contains reusable and convenient abstract classes to build Tkinter windows and frames around
    - `db.py` - Database logic, abstract classes for basic model definition and database interfacing
    - `config.py` - Project-wide configuration constants
- `init_db.py` - Simple script for database initialization, uses metadata from `toyshop.models` module
- `db.sqlite3` - Database file

### Graphical User Interface
The main building block for creating tkinter frame elements is the `FrameBase` class in the `util` module. Inheriting from it allows one to conveniently define child widgets in its `_init_frame()` method, allowing nested frame structure with the ability to efficiently implement and define own methods and/or variables.

<!-- When run as a module, the `__main__.py` is executed as an entrypoint, which then transfers execution to the `core` module.\ -->
The `core` module contains the main window implementation and logic, specifically the `MainWindow` class. The `MainWindow` has a special switching mechanism implemented which allows one to switch between different content frames (eg. Basket view, Checkout screen) without the need to create separate windows.\
To create your own content frame, create a subpackage for your feature in the `gui/` directory, create a new module file, inherit `util.FrameBase` or one of other convenience classes and reference it in the root `__init__.py`'s `CONTENT_WIDGETS` constant. Switch to your frame can be then carried out by calling the `load_content()` method of the root window. Frame instance lifetime is from load to load of another frame (unload of itself). Assume that the frame instance is always newly instantiated on load.
To access the root window instance, you can use a convenient `root_frame` property on your frame.

For inspiration on implementation check out modules in the `gui/order/` directory.

### Controllers
Controllers are objects which control and manage the flow of the real data. Controllers are to be created in the `controllers/` directory. Controllers should be implemented as singletons, which can be achieved by simply creating a new instance of your controller class at the end of the controller module. This way, you can import controllers on-demand as singletons with the `import` statement right from your GUI modules.

For practical example, check out the end of the `toyshop.controllers.order` module and one of the GUI models.

### Observer pattern
The `util.py` module contains special classes, implementing the Observer pattern, which allows you to dynamically update GUI by assigning a subject to it and getting notifications from your models, which implement the `ObservedObject` class. If used, don't forget to call the `destroy_frame()` method on each `Observer` object in the `_on_frame_destroy` callback method, which is called when unloading the current frame.

#### tldr;
- When creating a frame always inherit from `util.FrameBase` or one of its subclasses.
- Define your own GUI modules in a new directory in the `gui/` subpackage and reference their entrypoint classes in the root `__init__.py`'s `CONTENT_WIDGETS` constant.
- Load frame into window using root window's `load_content()` method.
- Define controllers in the `controllers/` subpackage.

### Models and Database
~~For now, database integration is still in a heavy development.~~ The database integration is in a mature state. Object models are defined in `models` module. If your model interacts with database in any way, it's encouraged to inherit the `db.DbBase` class. The `DbBase` implements universal `push()` method for updating records, `remove()` method and various class methods to get records from the database. Generally, you don't have to write any database related code with these methods.

The `DbBase` class inherits from the `ModelBase` class, which also implements a lot of convenient functionality. It has an implicit constructor, which automatically populates it's attributes, using the `attributes` class variable. It also has an advanced validation mechanism with the ability to load custom validation functions for each attribute.

 <!-- Most database calls and operations are to be implemented as class or instance methods on specific models. As an example, check out `Item` class' `get_all()` classmethod, which returns its class instance. For accessing database and making SQL queries, use `db.cursor()` in a `with` statement as described: -->

<!-- ```
with db.cursor() as cur:
    cur.execute("SQL STATEMENT")
``` -->

For debugging database outside of python, check out [DB Browser for SQLite](https://sqlitebrowser.org/).

<!-- ## TODO: -->
<!-- - [ ] check weird `Tk.Frame.forget` behaviour -->


<!-- ## License
This project's license is yet to be determined. -->