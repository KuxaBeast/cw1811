#!/bin/python
"""
Script for initialization of the database
"""
from sys import argv
import os
from toyshop import db, models as m

def init_model_tables():
    """Init tables."""
    print("Initializing tables")
    with db.cursor() as cur:
        for mdl in m.DB_MODELS:
            print(f"  Creating {mdl.__name__}")
            cur.execute(f"CREATE TABLE IF NOT EXISTS `{mdl.model_name}` {mdl.schema}")

def drop_all():
    """Recreate database."""
    print("Removing database")
    db.conn.close()
    os.remove(db.DATABASE)
    db.conn = db.db_factory()

def init_data(mdl_data):
    """Insert initial data."""
    print("Inserting data")
    with db.cursor() as cur:
        if isinstance(mdl_data, list):
            for mdl in mdl_data:
                mdl_dict = mdl.as_dict()
                cur.execute("INSERT INTO `{}` (`{}`) VALUES ({})".format(
                        mdl.model_name,
                        '`,`'.join(mdl_dict.keys()),
                        ','.join(":"+k for k in mdl_dict.keys())
                    ), mdl_dict
                )
        else:
            for mdl_name,data in mdl_data.items():
                if not isinstance(data, list):
                    data = [data]

                data = [x if not isinstance(x, db.ModelBase) else x.as_dict() for x in data]

                data_keys = data[0].keys()
                cur.executemany("INSERT INTO `{}` (`{}`) VALUES ({})".format(
                        mdl_name,
                        '`,`'.join(data_keys),
                        ','.join('?' for k in data_keys)
                    ), [list(val_dict.values()) for val_dict in data]
                )

if __name__ == "__main__":
    args = iter(argv[1:])

    if len(argv) < 2:
        print("""Use with at least one subcommand. Subcommands are executed in specified order.
Available subcommands:
    drop
    tables
    data""")

    while True:
        arg = next(args, None)

        if arg == "drop":
            drop_all()
        elif arg == "tables":
            init_model_tables()
        elif arg == "data":
            init_data(m.TEST_DATA)
        else:
            break
