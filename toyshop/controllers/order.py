"""
Order module implementing the 3rd feature.
"""
from toyshop.models import Basket, Item, Order
from toyshop.util import Singleton


class OrderController(metaclass=Singleton):
    """Controller singleton class to manage the order process."""
    def __init__(self):
        self._current_basket = None
        self._current_order = None
        self._last_order = None

        self._current_basket = Basket()

    @property
    def current_basket(self):
        """Currently active order instance."""
        return self._current_basket

    @property
    def current_order(self):
        """Currently active order instance."""
        if self._current_order is None:
            self._current_order = Order(self._current_basket)
        return self._current_order

    @property
    def last_order(self):
        """Last submitted order."""
        return self._last_order

    def submit_order(self):
        """Submit order, process payment and store the transaction in the database.
        Also reinitializes the basket."""
        # Submit the order instance
        self._current_order.submit()

        # Clean up and init new basket
        self._last_order = self._current_order
        self._current_order = None
        self._current_basket = Basket()
