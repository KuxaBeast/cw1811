"""
Entrypoint module.
"""
from toyshop.gui import order, admin
from toyshop.gui.core import MainWindow

CONTENT_WIDGETS = [
    *order.frames,
    *admin.frames,

    # Add your own content frames
]

if __name__ == "__main__":
    # Window initialization
    main_win = MainWindow()#default_load='BasketFrame')
    for frm in CONTENT_WIDGETS:
        main_win.register(frm)

    try:
        # Blocking GUI init function
        main_win.show()
    except KeyboardInterrupt:
        pass
