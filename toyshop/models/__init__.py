"""
Subpackage for models.
"""
from .item import Item, Category, ItemInOrder
from .order import Order, Basket
from .order_misc import DeliveryMethod, DeliveryType, CustomerAddress, DiscountCode

DB_MODELS = (Item, Category, Order, ItemInOrder, DeliveryMethod, CustomerAddress, DiscountCode)
TEST_DATA = [
    Category(name="mechanical"),
    Category(name="soft"),
    Category(name="toddlers"),

    Item(
        price=1, category_id=1, stock_amount=14, name="First Toy",
    ),
    Item(
        price=2, category_id=1, stock_amount=14, name="Second Toy",
    ),
    Item(
        price=3, category_id=1, stock_amount=14, name="Third Toy",
    ),
    Item(
        price=4, category_id=2, stock_amount=14, name="Fourth Toy",
    ),
    Item(
        price=5, category_id=2, stock_amount=14, name="Fifth Toy",
    ),
    Item(
        price=6, category_id=2, stock_amount=14, name="Sixth Toy",
    ),
    Item(
        price=7, category_id=3, stock_amount=14, name="Seventh Toy",
    ),
    Item(
        price=8, category_id=3, stock_amount=14, name="Eighth Toy",
    ),
    Item(
        price=9, category_id=3, stock_amount=14, name="Ninth Toy",
    ),

    DeliveryMethod(
        title="DPD classic",
        type=DeliveryType.DELIVERY,
        days_to_deliver=5,
        price="2.49",
        default_weight=2,
    ),
    DeliveryMethod(
        title="DPD express",
        type=DeliveryType.DELIVERY,
        price="3.99",
        days_to_deliver=3,
        default_weight=1,
    ),
    DeliveryMethod(
        title="Personal Collection",
        type=DeliveryType.COLLECTION,
        price="0.99",
    ),
]

# TEST_DATA = {
#     "category": {
#         "name": "mechanical"
#     },
#     "item": [
#         # {
#         #     "name": "First Toy",
#         #     "price": 5,
#         #     "description": "A very nice toy.",
#         #     "category_id": 1
#         # },
#         # {
#         #     "name": "Second Toy",
#         #     "price": 19,
#         #     "description": "Another very nice toy.",
#         #     "category_id": 1
#         # }
#         Item(price=1, name="First Toy"),
#         Item(price=2, name="Second Toy"),
#         Item(price=3, name="Third Toy"),
#         Item(price=4, name="Fourth Toy"),
#         Item(price=5, name="Fifth Toy"),
#         Item(price=6, name="Sixth Toy"),
#         Item(price=7, name="Seventh Toy"),
#         Item(price=8, name="Eighth Toy"),
#         Item(price=9, name="Ninth Toy")
#     ],
#     "delivery_method": [
#         {
#             "title": "DPD express",
#             "type": DeliveryType.DELIVERY.value,
#             "price": "1.99",
#             "days_to_deliver": 1,
#             "default_weight": 1
#         },
#         {
#             "title": "Personal Collection",
#             "type": DeliveryType.COLLECTION.value,
#             "price": "0.19",
#             "days_to_deliver": 3,
#             "default_weight": 0
#         },
#     ],
#     "discountcode": [
#         {
#             "title": "Weekend deal",
#             "code": "WKND35",
#             "type": DiscountType.PERCENT10.value,
#             "item_category": 1,
#             "start_timestamp": int(datetime.now().timestamp()), #"datetime('now')",
#             "end_timestamp": int((datetime.now()+timedelta(days=2)).timestamp()) #"datetime('now','+10 days')"
#         },
#         {
#             "title": "Black Friday",
#             "code": "BLKF13",
#             "type": DiscountType.BOGOF.value,
#             "item_category": 1,
#             "start_timestamp": int(datetime.now().timestamp()), #"datetime('now')",
#             "end_timestamp": int((datetime.now()+timedelta(days=2)).timestamp()) #"datetime('now','+10 days')"
#         }
#     ]
# }