"""
Models of Item and alike.
"""
from toyshop.config import CURRENCY
from toyshop.db import DbBase, MultiPkDbBase
from toyshop.util import ObservedObject


class Category(DbBase):
    """Model class for Category object"""
    schema = """(
    "id"    INTEGER NOT NULL,
    "name"  TEXT NOT NULL,
    PRIMARY KEY("id" AUTOINCREMENT)
)"""
    model_name = "category"
    attributes = ['name']

    def __repr__(self):
        return f"<Category #{self.id}: \"{self.name}\">"


class Item(DbBase):
    """Model class for Item object"""
    schema = """(
    "id"    INTEGER NOT NULL,
    "name"  TEXT NOT NULL,
    "price" NUMERIC NOT NULL,
    "description"   TEXT,
    "category_id"   INTEGER,
    "stock_amount"    INTEGER NOT NULL DEFAULT 0,
    PRIMARY KEY("id" AUTOINCREMENT),
    FOREIGN KEY("category_id") REFERENCES "category"("id")
)"""
    model_name = "item"
    attributes = ['name', 'price', 'description', 'category_id', 'stock_amount']

    def __init__(self, *args, **kwargs):
        super().__init__(**kwargs)
        if self._stock_amount is None:
            self._stock_amount = 0

    def __repr__(self):
        return f"<Item #{self.id}: \"{self.name}\" for {self.price}{CURRENCY}>"


class ItemInOrder(ObservedObject, MultiPkDbBase):
    """Model class for items associated with an order."""
    schema = """(
    "item_id"   INTEGER NOT NULL,
    "order_id"  INTEGER NOT NULL,
    "price"    NUMERIC NOT NULL,
    "amount"    INTEGER NOT NULL DEFAULT 1,
    FOREIGN KEY("item_id") REFERENCES "item"("id"),
    FOREIGN KEY("order_id") REFERENCES "order"("id"),
    PRIMARY KEY("order_id","item_id")
)"""
    model_name = "item_in_order"
    attributes = ['item_id', 'order_id', 'price', 'amount']

    id_columns = ('item_id', 'order_id')

    def __init__(self, item: Item = None, order=None, *args, **kwargs):
        self._item = item
        self._order = order

        super().__init__(**kwargs)
        if self._amount is None:
            self._amount = 1

    @property
    def order(self):
        """Assigned `Order` instance."""
        return self._order
    @order.setter
    def order(self, val):
        self._order = val

    @property
    def item(self):
        """Assigned `Item` instance."""
        return self._item

    @property
    def price(self):
        """Attribute override. Returns assigned `Item`'s price
        if the internal parameter is not set."""
        return self._price or (self._item and self._item.price)

    @property
    def item_id(self):
        """Attribute override. Returns assigned `Item`'s id."""
        return self._item.id if self._item else self._item_id

    @property
    def order_id(self):
        """Attribute override. Returns assigned `Order`'s id."""
        return self._order.id if self._order else self._order_id

    def total(self):
        """Return total price."""
        return self.price and self.amount * self.price

    def __repr__(self):
        return f"""<ItemInOrder from {self._order}, being {
            self._item}, amount: {self.amount}, real_price: {self.price}>"""

    def __str__(self):
        return f"""{self.item.name.ljust(22)} {
            f'{self.amount}x'.ljust(3)} {str(self.price).rjust(5)} {CURRENCY}"""
