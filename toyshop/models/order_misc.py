"""
Models associated with order.
"""
from enum import Enum
from typing import List, Tuple

from toyshop.config import CURRENCY
from toyshop.db import DbBase

from .item import Item


class DiscountType(Enum):
    """Discount type enumeration."""
    PERCENT10 = 0
    BOGOF = 1

class DiscountCode(DbBase):
    schema = """(
    "id"    INTEGER NOT NULL,
    "title" TEXT NOT NULL,
    "code"  TEXT NOT NULL UNIQUE,
    "type"  INTEGER NOT NULL,
    "item_category" INTEGER NOT NULL,
    "start_timestamp"   INTEGER NOT NULL,
    "end_timestamp" INTEGER NOT NULL,
    FOREIGN KEY("item_category") REFERENCES "category"("id"),
    PRIMARY KEY("id" AUTOINCREMENT)
)"""
    model_name = "discountcode"
    attributes = ['title', 'code', 'type', 'item_category', 'start_timestamp', 'end_timestamp']

    def calculate(self, items: List[Tuple[Item, int]]):
        """Calculate total discount for supplied list of items."""
        raise NotImplementedError

    @classmethod
    def get_by_code(cls, code: str):
        """Retrieve a discount code by the code value."""
        return cls.get_one_where(code=code)


class DeliveryType(Enum):
    """Delivery type enumeration."""
    COLLECTION = 0
    DELIVERY = 1
    FOREIGN_DELIVERY = 5

class DeliveryMethod(DbBase):
    """Model class for Delivery object"""
    schema = """(
    "id"    INTEGER NOT NULL,
    "title" TEXT NOT NULL,
    "type" INTEGER NOT NULL,
    "price" NUMERIC NOT NULL,
    "days_to_deliver"   INTEGER,
    "default_weight"   INTEGER NOT NULL DEFAULT 0,
    PRIMARY KEY("id" AUTOINCREMENT)
)"""
    model_name = "delivery_method"
    attributes = ['title', 'type', 'price', 'days_to_deliver', 'default_weight']

    def __init__(self, *args, **kwargs):
        super().__init__(**kwargs)
        if self._default_weight is None:
            self._default_weight = 0

    @property
    def type(self):
        """Assigned `DeliveryType`."""
        return self._type and self._type.value
    @type.setter
    def type(self, val):
        self._type = val if isinstance(val, DeliveryType) else DeliveryType(val)

    @staticmethod
    def find_priority_default(listset):
        """Find the method with the highest priority, to be assigned as default."""
        return sorted(listset, key=lambda x: x.default_weight, reverse=True)[0]

    def __repr__(self):
        return f"<DeliveryMethod \
#{self.id}: \"{self.title}\" of {self.type} for {self.price}{CURRENCY}, \
takes {self.days_to_deliver} days>"

    def __str__(self):
        return f"""{("Delivery:" if self.type is DeliveryType.DELIVERY
            else "Personal collection:").ljust(28)} {str(self.price).rjust(5)} {CURRENCY}"""

class CustomerAddress(DbBase):
    """Model class for Order object"""
    schema = """(
    "id"    INTEGER NOT NULL,
    "customer_id"   INTEGER,
    "first_name"  TEXT NOT NULL,
    "last_name"  TEXT NOT NULL,
    "street"    TEXT NOT NULL,
    "postcode"  TEXT NOT NULL,
    "city"  TEXT NOT NULL,
    "country"   TEXT NOT NULL,
    PRIMARY KEY("id" AUTOINCREMENT),
    FOREIGN KEY("customer_id") REFERENCES "customer"("id")
)"""
    model_name = "customer_address"
    attributes = ['first_name', 'last_name', 'street', 'postcode', 'city', 'country']

    def __repr__(self):
        return f"""<CustomerAddress #{self.id}: {
            self.first_name} {self.last_name}, {self.street}, {
            self.postcode} {self.city}, {self.country}>"""
