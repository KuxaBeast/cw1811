"""
Main models for order functionality.
"""
from datetime import datetime
from typing import List, Union

from toyshop.config import CURRENCY
from toyshop.db import DbBase, ModelBase
from toyshop.util import ObservedObject

from .item import Item, ItemInOrder
from .order_misc import CustomerAddress, DeliveryMethod


class Basket(ObservedObject):
    """Model to represent items in basket."""
    def __init__(self, items: List = None):
        self._items = [ItemInOrder(item) for item in (items or [])]

    @property
    def items(self):
        """All present items."""
        return self._items

    @property
    def total(self):
        """Total value of present items."""
        return sum(map(ItemInOrder.total, self._items))

    def get_item_record(self, item: Union[ItemInOrder, Item, int]):
        """Check if ItemInOrder is in basket and return its instance."""
        if isinstance(item, ItemInOrder) and item in self._items:
            return item

        return next(filter(
            lambda i: i.item_id == (
                item.item_id if isinstance(item, ItemInOrder)
                else (item.id if isinstance(item, Item) else item)
            ),
            self._items
        ), None)

    def add_item(self, item: Union[ItemInOrder, Item, int]):
        """Add an item to the basket. Accepts Item object or its ID."""
        record = self.get_item_record(item)
        if record is not None:
            record.amount += 1
        else:
            self._items.append(
                item if isinstance(item, ItemInOrder)
                else (
                    ItemInOrder(item) if isinstance(item, Item)
                    else ItemInOrder(Item.get(item))
                )
            )

        self._notify()

    def remove_item(self, item: Union[ItemInOrder, Item, int], remove_all=False):
        """Remove an item from the basket. Accepts Item object or its ID."""
        record = self.get_item_record(item)
        if record is None:
            return

        if not remove_all and record.amount > 1:
            record.amount -= 1
        else:
            self._items.remove(record)

        self._notify()

    def items_assign_order(self, order):
        """Assign the current order to items."""
        for item in self._items:
            item.order = order

    def submit(self, order):
        """Submit basket items into database."""
        # Associate self with basket items
        self.items_assign_order(order)

        failed = 0

        # Store in database
        for item in self._items:
            failed += not item.push()

        if failed:
            return failed

        # Dirty way of decreasing of the stock amount
        # Should be handled only once and properly in SQL, should be done in one transaction.
        for item in self._items:
            item.item.stock_amount -= item.amount
            failed += not item.item.push()

        return failed

    def __repr__(self):
        return str(self.items)

    def __str__(self):
        out = [
            f"Basket: {len(self._items)} items",
            *[f"  {item}" for item in self._items],
            f"{'Basket total:'.ljust(28)} {str(self.total).rjust(5)} {CURRENCY}"
        ]
        return "\n".join(out)


class Order(ObservedObject, DbBase):
    """Model class for Order object."""
    schema = """(
    "id"    INTEGER NOT NULL,
--    "customer_id"   INTEGER,
    "shipping_address_id"   INTEGER NOT NULL,
    "delivery_method_id"    INTEGER NOT NULL,
    "order_timestamp"   INTEGER NOT NULL,
    "paid"  BOOLEAN NOT NULL DEFAULT 0,
    "returned"  BOOLEAN NOT NULL DEFAULT 0,
    PRIMARY KEY("id" AUTOINCREMENT),
--    FOREIGN KEY("customer_id") REFERENCES "customer"("id"),
    FOREIGN KEY("shipping_address_id") REFERENCES "customer_address"("id"),
    FOREIGN KEY("delivery_method_id") REFERENCES "delivery_method"("id")
)"""
    model_name = "order"
    attributes = [
        'shipping_address_id', 'delivery_method_id', 'order_timestamp', 'paid', 'returned'
    ]

    def __init__(self, basket: Basket, *args, delivery_method=None, address=None, **kwargs):
        self._base_basket = basket
        super().__init__(*args, **kwargs)
        self._delivery = delivery_method
        # self._discount = discount
        self._address = address

        self._delivery_options = None
        self._update_delivery_options()

        self._payment_info = None

    @property
    def basket(self):
        """Assigned `Basket` instance."""
        return self._base_basket

    def _update_delivery_options(self):
        """Update set of accessible delivery options."""
        new_opts = list(DeliveryMethod.get_all())
        if self._delivery_options == new_opts:
            return

        self._delivery_options = new_opts
        if not self._delivery in self._delivery_options:
            self._delivery = DeliveryMethod.find_priority_default(self._delivery_options)

        self._notify() # in init no observers attached, so does nothing

    @property
    def delivery_options(self):
        """Accessible delivery options."""
        return self._delivery_options

    @property
    def delivery(self):
        """Selected delivery method."""
        return self._delivery
    @delivery.setter
    def delivery(self, val: Union[DeliveryMethod, int]):
        # if value is int, assume id and retrieve method from `delivery_options`.
        if isinstance(val, int):
            val = next((x for x in self._delivery_options if x.id == val))

        self._delivery = val
        self._notify()

    @property
    def address(self):
        """`Address` instance."""
        if self._address is None:
            self._address = CustomerAddress()
        return self._address

    @property
    def payment_info(self):
        """`PaymentInfo` instance."""
        if self._payment_info is None:
            self._payment_info = PaymentInfo()
        return self._payment_info

    @property
    def order_timestamp(self):
        """Timestamp of creation."""
        return int(self._order_timestamp.timestamp())
    @order_timestamp.setter
    def order_timestamp(self, val):
        self._order_timestamp = val if isinstance(val, datetime) else datetime.fromtimestamp(val)

    @property
    def shipping_address_id(self):
        """Id of the associated shipping address."""
        return self._address.id if self._address else self._shipping_address_id

    @property
    def delivery_method_id(self):
        """Id of the associated delivery method."""
        return self._delivery.id if self._delivery else self._delivery_method_id

    @property
    def checkout_summary(self):
        """Summary values for checkout."""
        ret = (
            self.basket.total,
            self.delivery and self.delivery.price or 0,
            # self.discount and self.discount.total or 0,
        )
        return (*ret, sum(ret))

    def save_receipt(self, path):
        """Write the receipt to a file."""
        with open(path, 'w') as f:
            f.write(self.get_receipt())
        print(f'saved to {path}')

    def get_receipt(self):
        """Receipt generator method. Generated from other objects' `__str__()` methods."""
        return "\n".join([
            "ALL ABOUT TOYS".center(40),
            f"Date: {self._order_timestamp.strftime('%d %b %Y %H:%M:%S')}",
            str(self.basket),
            str(self.delivery),
            f"""{'Paid by card:'.ljust(28)} {
                str(self.basket.total + self.delivery.price).rjust(5)
                } {CURRENCY}"""
        ])

    def submit(self):
        """Submit order."""
        # Payment
        self.paid = self.payment_info.process_payment()
        self.returned = False
        self.order_timestamp = datetime.now()

        self.address.push()
        self.push()
        self.basket.submit(self)


class PaymentInfo(ModelBase):
    """Model representing payment card information."""
    attributes = ['card_number', 'exp_month', 'exp_year', 'security', 'cardholder']

    # custom validation methods, invoked from `ModelBase`'s  `validate()` method
    card_number_validate = lambda self, val: self._int_len_check(val, 16)
    exp_month_validate = lambda self, val: self._int_len_check(val, 2) and int(val) in range(1, 13)
    exp_year_validate = lambda self, val: self._int_len_check(val, 2)
    security_validate = lambda self, val: self._int_len_check(val, 3, 4)

    def _int_len_check(self, val: str, length: int, max_len: int = None):
        """Number length verification function."""
        try:
            int(val)
            assert len(val) >= length and len(val) <= (max_len if max_len else length)
        except:
            return False
        return True

    def process_payment(self):
        """VISACheck emulator function."""
        if not True:
            raise Exception("Payment did not get through.")
        return True
