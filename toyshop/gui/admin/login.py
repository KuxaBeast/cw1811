import tkinter as tk
from tkinter import messagebox

from toyshop.util import FrameBase


class LoginFrame(FrameBase):
    def _init_frame(self, *args, **kwargs):
        title = tk.Label(self, text="Login to access administrator panel.")
        title.grid(row=0, column=0)
        # input fields for username and password as well as a button to submit details
        # Username
        userIDLabel = tk.Label(self, text="Username :")
        userIDLabel.grid(row=1, column=0)
        userIDEntry = tk.Entry(self)
        userIDEntry.grid(row=1, column=1)

        # Password
        passIDLabel = tk.Label(self, text="Password :")
        passIDLabel.grid(row=2, column=0)
        passIDEntry = tk.Entry(self, show="*")
        passIDEntry.grid(row=2, column=1)

        submitButton = tk.Button(self, text="Login",
                                 command=lambda: self.gotoadmin(userIDEntry.get(), passIDEntry.get()))
        submitButton.grid(row=2, column=3)

    def gotoadmin(self, uid, pid):
        loginCheck(uid, pid)
        if loginCheck(uid, pid):
            self.root_frame.load_content('AdminFrame')
        else:
            messagebox.showerror(title="Login Attempt Failed", message="Username or password did not match. Try "
                                                                       "again.")


def loginCheck(uid, pid):
    if uid == "admin" and pid == "admin":
        return True
    else:
        return False
