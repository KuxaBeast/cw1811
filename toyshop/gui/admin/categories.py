import tkinter as tk
from tkinter import messagebox, ttk

from toyshop.models import Category
from toyshop.util import FrameBase


class CategoryFrame(FrameBase):
    def _init_frame(self, *args, **kwargs):
        title = tk.Label(self, text="Categories")
        title.grid(row=0, column=0)

        # Shows everything in database
        alist = list(Category.get_all())
        dblist = ttk.Treeview(self, columns=(1, 2), show="headings", height="10")
        dblist.grid(row=1, column=0)
        dblist.heading(1, text="ID")
        dblist.heading(2, text="Name")

        for i in alist:
            dblist.insert('', 'end', values=[i.id, i.name])

        # Buttons
        addCatButton = tk.Button(self, text="Add category", command=lambda: self.loadAddFrame())
        addCatButton.grid(row=2, column=1)

        editCatButton = tk.Button(self, text="Edit category", command=lambda: self.loadEditFrame())
        editCatButton.grid(row=3, column=1)

        deleteCatButton = tk.Button(self, text="Delete category", command=lambda: self.loadDeleteFrame())
        deleteCatButton.grid(row=4, column=1)

        adminpanelButton = tk.Button(self, text="Back to admin panel", command=lambda: self.loadAdminFrame())
        adminpanelButton.grid(row=5, column=1)

    def loadAddFrame(self):
        self.root_frame.load_content('AddCategoryFrame')

    def loadEditFrame(self):
        self.root_frame.load_content('EditCategoryFrame')

    def loadDeleteFrame(self):
        self.root_frame.load_content('DeleteCategoryFrame')

    def loadAdminFrame(self):
        self.root_frame.load_content('AdminFrame')


class AddCategoryFrame(FrameBase):
    def _init_frame(self, *args, **kwargs):
        title = tk.Label(self, text="Add a new category")
        title.grid(row=0, column=0)
        nameLabel = tk.Label(self, text="Category Name: ")
        nameLabel.grid(row=1, column=0)
        catName = tk.Entry(self)
        catName.grid(row=1, column=1)
        addButton = tk.Button(self, text="Add category", command=lambda: self.addCategory(catName.get()))
        addButton.grid(row=1, column=2)
        backButton = tk.Button(self, text="Back to categories", command=lambda: self.backtocategories())
        backButton.grid(row=2, column=2)

    def backtocategories(self):
        self.root_frame.load_content('CategoryFrame')

    def addCategory(self, name):
        if Category.get_one_where(name=name):
            messagebox.showerror(title="Failed", message="Category (" + name + ") already exists.")
        else:
            newCat = Category(name=name)
            newCat.push()
            self.backtocategories()


class EditCategoryFrame(FrameBase):
    def _init_frame(self, *args, **kwargs):
        title = tk.Label(self, text="Edit a category")
        title.grid(row=0, column=0)

        catid = tk.Label(self, text="Category ID: ")
        catid.grid(row=1, column=0)
        catidentry = tk.Entry(self)
        catidentry.grid(row=1, column=1)

        newNameLabel = tk.Label(self, text="New category name: ")
        newNameLabel.grid(row=2, column=0)
        newCatName = tk.Entry(self)
        newCatName.grid(row=2, column=1)

        editButton = tk.Button(self, text="Edit category",
                               command=lambda: self.editCategory(catidentry.get(), newCatName.get()))
        editButton.grid(row=2, column=2)

        backButton = tk.Button(self, text="Back to categories", command=lambda: self.backtocategories())
        backButton.grid(row=3, column=2)

    def backtocategories(self):
        self.root_frame.load_content('CategoryFrame')

    def editCategory(self, catid, newname):
        if Category.get_one_where(id=catid):
            editcat = Category(id=catid, name=newname)
            editcat.push(insert=False)
            self.backtocategories()
        else:
            messagebox.showerror(title="Failed", message=catid+" does not exist.")


class DeleteCategoryFrame(FrameBase):
    def _init_frame(self, *args, **kwargs):
        title = tk.Label(self, text="Delete a category")
        title.grid(row=0, column=0)

        idLabel = tk.Label(self, text="Category ID: ")
        idLabel.grid(row=1, column=0)
        idEntry = tk.Entry(self)
        idEntry.grid(row=1, column=1)

        deleteButton = tk.Button(self, text="Delete category",
                                 command=lambda: self.deleteCategory(idEntry.get()))
        deleteButton.grid(row=3, column=2)

        backButton = tk.Button(self, text="Back to categories", command=lambda: self.backtocategories())
        backButton.grid(row=4, column=2)

    def backtocategories(self):
        self.root_frame.load_content('CategoryFrame')

    def deleteCategory(self, catID):
        if Category.get_one_where(id=catID):
            delCat = Category(id=catID)
            delCat.remove()
            self.backtocategories()
        else:
            messagebox.showerror(title="Failed", message="Category ID did not match to an existing one.")
