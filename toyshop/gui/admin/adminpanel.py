import tkinter as tk
from datetime import datetime

from toyshop.gui.admin import stock
from toyshop.util import FrameBase


class AdminFrame(FrameBase):
    def _init_frame(self, *args, **kwargs):
        title = tk.Label(self, text="Welcome")
        title.grid(row=0, column=1)

        catButton = tk.Button(self, text="Categories", command=lambda: self.loadCategories())
        catButton.grid(row=1, column=0)

        prodButton = tk.Button(self, text="Products", command=lambda: self.loadProducts())
        prodButton.grid(row=1, column=1)

        stockButton = tk.Button(self, text="Stock", command=lambda: self.loadStock())
        stockButton.grid(row=1, column=2)

    def loadCategories(self):
        self.root_frame.load_content('CategoryFrame')

    def loadProducts(self):
        self.root_frame.load_content('ProductFrame')

    def loadStock(self):
        self.root_frame.load_content('StockFrame')
