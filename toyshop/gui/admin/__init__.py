from .categories import CategoryFrame, AddCategoryFrame, EditCategoryFrame, DeleteCategoryFrame
from .login import LoginFrame
from .adminpanel import AdminFrame
from .products import ProductFrame, AddProductFrame, EditProductFrame, DeleteProductFrame
from .stock import StockFrame

frames = [
    LoginFrame,
    AdminFrame,
    CategoryFrame,
    AddCategoryFrame,
    EditCategoryFrame,
    DeleteCategoryFrame,
    ProductFrame,
    AddProductFrame,
    EditProductFrame,
    DeleteProductFrame,
    StockFrame
]
