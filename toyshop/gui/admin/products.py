import tkinter as tk
from tkinter import messagebox, ttk

from toyshop.models import Item
from toyshop.util import FrameBase


class ProductFrame(FrameBase):
    def _init_frame(self, *args, **kwargs):
        title = tk.Label(self, text="Products")
        title.grid(row=0, column=0)

        # Treeview
        alist = list(Item.get_all())
        dblist = ttk.Treeview(self, columns=(1, 2, 3, 4, 5, 6), show="headings", height="10")
        dblist.grid(row=1, column=0)
        dblist.heading(1, text="ID")
        dblist.heading(2, text="Name")
        dblist.heading(3, text="Price")
        dblist.heading(4, text="Stock amount")
        dblist.heading(5, text="Category ID")
        dblist.heading(6, text="Description")

        for i in alist:
            dblist.insert('', 'end', values=[i.id, i.name, i.price, i.stock_amount, i.category_id, i.description])

        # Buttons
        addCatButton = tk.Button(self, text="Add product", command=lambda: self.loadAddFrame())
        addCatButton.grid(row=2, column=1)

        editCatButton = tk.Button(self, text="Edit product", command=lambda: self.loadEditFrame())
        editCatButton.grid(row=3, column=1)

        deleteCatButton = tk.Button(self, text="Delete product", command=lambda: self.loadDeleteFrame())
        deleteCatButton.grid(row=4, column=1)

        adminpanelButton = tk.Button(self, text="Back to admin panel", command=lambda: self.loadAdminFrame())
        adminpanelButton.grid(row=5, column=1)

    def loadAddFrame(self):
        self.root_frame.load_content('AddProductFrame')

    def loadEditFrame(self):
        self.root_frame.load_content('EditProductFrame')

    def loadDeleteFrame(self):
        self.root_frame.load_content('DeleteProductFrame')

    def loadAdminFrame(self):
        self.root_frame.load_content('AdminFrame')


class AddProductFrame(FrameBase):
    def _init_frame(self, *args, **kwargs):
        title = tk.Label(self, text="Add a new product")
        title.grid(row=0, column=0)
        nameLabel = tk.Label(self, text="*Product Name: ")
        nameLabel.grid(row=1, column=0)
        prodName = tk.Entry(self)
        prodName.grid(row=1, column=1)

        priceLabel = tk.Label(self, text="*Product Price: ")
        priceLabel.grid(row=2, column=0)
        prodPrice = tk.Entry(self, textvariable=tk.DoubleVar())
        prodPrice.grid(row=2, column=1)

        stockLabel = tk.Label(self, text="*Stock Amount: ")
        stockLabel.grid(row=3, column=0)
        prodStock = tk.Entry(self, textvariable=tk.IntVar())
        prodStock.grid(row=3, column=1)

        catLabel = tk.Label(self, text="*Category ID: ")
        catLabel.grid(row=4, column=0)
        prodCatId = tk.Entry(self, textvariable=tk.IntVar())
        prodCatId.grid(row=4, column=1)

        descLabel = tk.Label(self, text="Description")
        descLabel.grid(row=5, column=0)
        prodDesc = tk.Entry(self)
        prodDesc.grid(row=5, column=1)

        addButton = tk.Button(self, text="Add product",
                              command=lambda: self.addProduct(prodName.get(), prodPrice.get(), prodStock.get(),
                                                              prodCatId.get(), prodDesc.get()))
        addButton.grid(row=1, column=2)
        backButton = tk.Button(self, text="Back to products", command=lambda: self.backtoproducts())
        backButton.grid(row=2, column=2)

    def backtoproducts(self):
        self.root_frame.load_content('ProductFrame')

    def addProduct(self, name, price, stock, catid, desc):
        if name == "" or price == 0 or stock == 0 or catid == 0:
            messagebox.showerror(title="Failed", text="One or more fields are missing. Please fill all fields with *")
        else:
            newItem = Item(name=str(name), price=float(price), descirption=str(desc),
                           category_id=int(catid), stock_amount=int(stock))
            newItem.push()
            self.root_frame.load_content('ProductFrame')


class EditProductFrame(FrameBase):
    def _init_frame(self, *args, **kwargs):
        title = tk.Label(self, text="Edit a product (All fields must be filled.)")
        title.grid(row=0, column=0)

        prodid = tk.Label(self, text="Product ID (to be edited): ")
        prodid.grid(row=1, column=0)
        prodidentry = tk.Entry(self)
        prodidentry.grid(row=1, column=1)

        newNameLabel = tk.Label(self, text="New product name: ")
        newNameLabel.grid(row=2, column=0)
        newProdName = tk.Entry(self)
        newProdName.grid(row=2, column=1)

        newPriceLabel = tk.Label(self, text="New price: ")
        newPriceLabel.grid(row=3, column=0)
        newProdPrice = tk.Entry(self)
        newProdPrice.grid(row=3, column=1)

        newStockLabel = tk.Label(self, text="New stock: ")
        newStockLabel.grid(row=4, column=0)
        newProdStock = tk.Entry(self)
        newProdStock.grid(row=4, column=1)

        newCatLabel = tk.Label(self, text="New category ID: ")
        newCatLabel.grid(row=5, column=0)
        newProdCat = tk.Entry(self)
        newProdCat.grid(row=5, column=1)

        newDescLabel = tk.Label(self, text="New description: ")
        newDescLabel.grid(row=6, column=0)
        newProdDesc = tk.Entry(self)
        newProdDesc.grid(row=6, column=1)



        editButton = tk.Button(self, text="Edit product",
                               command=lambda: self.editProduct(prodidentry.get(),
                                                                newProdName.get(),
                                                                newProdPrice.get(),
                                                                newProdStock.get(),
                                                                newProdCat.get(),
                                                                newProdDesc.get()))
        editButton.grid(row=2, column=2)

        backButton = tk.Button(self, text="Back to categories", command=lambda: self.backtoproducts())
        backButton.grid(row=3, column=2)

    def backtoproducts(self):
        self.root_frame.load_content('ProductFrame')

    def editProduct(self, prodid, newname, newprice, newstock, newcatid, newdesc):
        if Item.get_one_where(id=prodid):
            editprod = Item(id=prodid, name=newname, price=newprice, stock_amount=newstock, category_id=newcatid, description=newdesc)
            editprod.push(insert=False)
            self.backtoproducts()
        else:
            messagebox.showerror(title="Failed", message=prodid + " does not exist.")


class DeleteProductFrame(FrameBase):
    def _init_frame(self, *args, **kwargs):
        title = tk.Label(self, text="Delete a product")
        title.grid(row=0, column=0)

        prodidLabel = tk.Label(self, text="Product ID: ")
        prodidLabel.grid(row=1, column=0)
        prodidEntry = tk.Entry(self)
        prodidEntry.grid(row=1, column=1)

        deleteButton = tk.Button(self, text="Delete product",
                                 command=lambda: self.deleteProduct(prodidEntry.get()))
        deleteButton.grid(row=3, column=2)

        backButton = tk.Button(self, text="Back to products", command=lambda: self.backtoproducts())
        backButton.grid(row=4, column=2)

    def backtoproducts(self):
        self.root_frame.load_content('ProductFrame')

    def deleteProduct(self, prodID):
        if Item.get_one_where(id=prodID):
            delProd = Item(id=prodID)
            delProd.remove()
            self.backtoproducts()
        else:
            messagebox.showerror(title="Failed", message="Product ID did not match to an existing one.")
