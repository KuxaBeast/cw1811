import tkinter as tk
from datetime import date
from tkinter import messagebox

from toyshop.models import Item
from toyshop.util import FrameBase


class StockFrame(FrameBase):
    def _init_frame(self, *args, **kwargs):
        title = tk.Label(self, text="Stock Management")
        title.grid(row=0, column=0)

        toFileButton = tk.Button(self, text="Save Products to file", command=lambda: self.saveStockToFile())
        toFileButton.grid(row=1, column=0)

        backButton = tk.Button(self, text="Back", command=lambda: self.backtoadmin())
        backButton.grid(row=1, column=1)

    def backtostock(self):
        self.root_frame.load_content('StockFrame')

    def backtoadmin(self):
        self.root_frame.load_content('AdminFrame')

    def saveStockToFile(self):
        today = str(date.today())
        newfile = open(today + " ~ total stock.txt", "w")
        newfile.write("Total product stock\n-----------------\n")
        items = list(Item.get_all())
        for i in items:
            newfile.write("%s\n" % i)
        newfile.close()
        messagebox.showinfo(title="Product Stock Saved",
                            message="Product stock has been saved as: " + today + " ~ total stock.txt")

    def saveStockUnder20(self):
        today = str(date.today())
        newfile = open(today + " ~ stock under 20.txt", "w")
        newfile.write("Products under 20\n-----------------\n")
        items = list(Item.get_all())
        for i in items:
            if i.stock_amount in range(0, 21):
                Item.get_where(stock_amount=i)
                newfile.write("%s\n" % i)

            else:
                pass
        newfile.close()


def alert():
    stockcheck = StockFrame()
    stockcheck.saveStockUnder20()
    today = str(date.today())
    messagebox.showwarning(title="Stock Notification", message="Products in stock which have less than 20 "
                                                               "items on sale. Item report saved as: "
                                                               + today + " ~ stock under 20.txt")