"""
Payment module.
"""
import tkinter as tk
import traceback
from tkinter import messagebox

from toyshop.controllers.order import OrderController
from toyshop.util import FrameBase, Observer, StorableObserver


class PaymentFrame(FrameBase):
    def _init_frame(self):
        # ROW 0
        self.title = tk.Label(self, text='Payment', font=self.root_frame.font_title)
        self.title.grid(column=0, row=0, columnspan=2, sticky='w', padx=4, pady=4)

        # ROW 1
        self.subtext = tk.Label(self,
            text='Please enter your payment information. All fields must be filled in.')
        self.subtext.grid(column=0, row=1, columnspan=2, sticky='w', padx=4, pady=4)

        # ROW 2
        self.payment = PaymentSubFrame(
            self, subject=OrderController().current_order.payment_info)
        self.payment.grid(column=0, row=2, columnspan=2, rowspan=1, sticky='')

        # ROW 3
        self.prev_button = tk.Button(self, text='Previous', command=self.goto_address)
        self.prev_button.grid(column=0, row=3, sticky='w', padx=2, pady=2)
        self.next_button = tk.Button(self, text='Confirm payment', command=self.goto_finish)
        self.next_button.grid(column=1, row=3, sticky='e', padx=2, pady=2)

        self.columnconfigure(1, weight=1)
        self.rowconfigure(2, weight=1)

        self.configure(padx=2, pady=2)

    def goto_address(self):
        self.root_frame.load_content('AddressFrame')

    def goto_finish(self):
        self.payment.save_content()
        if not self.payment.subject.validate():
            messagebox.showerror("Validation Error", "Please check your input and try again.")
            return

        # Submit if doesn't error out
        try:
            OrderController().submit_order()
        except:
            traceback.print_exc()
            return

        self.root_frame.load_content('SummaryFrame')

    def _on_frame_destroy(self):
        self.payment.frame_destroy()


class PaymentSubFrame(StorableObserver, FrameBase):
    def _init_frame(self):
        self.variables = {
            'card_number': tk.StringVar(),
            'exp_month': tk.StringVar(value="00"),
            'exp_year': tk.StringVar(value="00"),
            'security': tk.StringVar(),
            'cardholder': tk.StringVar(),
        }

        self.card_number_label = tk.Label(self, text='Card number: ')
        self.card_number = tk.Entry(self, textvariable=self.variables['card_number'], width=16)
        self.expiration_label_fst = tk.Label(self, text='Expiration (MM/YY): ')
        self.expiration_month = tk.Entry(self, textvariable=self.variables['exp_month'], width=2)
        self.expiration_label_sec = tk.Label(self, text='/')
        self.expiration_year = tk.Entry(self, textvariable=self.variables['exp_year'], width=2)
        self.security_code_label = tk.Label(self, text='CVV: ')
        self.security_code = tk.Entry(self, textvariable=self.variables['security'], width=4)
        self.cardholder_label = tk.Label(self, text='Cardholder: ')
        self.cardholder = tk.Entry(self, textvariable=self.variables['cardholder'])

        self.card_number.bind("<FocusOut>", lambda event: self.save_content('card_number'))
        self.expiration_month.bind("<FocusOut>", lambda event: self.save_content('exp_month'))
        self.expiration_year.bind("<FocusOut>", lambda event: self.save_content('exp_year'))
        self.security_code.bind("<FocusOut>", lambda event: self.save_content('security'))
        self.cardholder.bind("<FocusOut>", lambda event: self.save_content('cardholder'))

        self.card_number_label.grid(row=0, column=0, sticky='e')
        self.card_number.grid(row=0, column=1, columnspan=5, sticky='we')
        self.expiration_label_fst.grid(row=1, column=0, sticky='e')
        self.expiration_month.grid(row=1, column=1, columnspan=1, sticky='we')
        self.expiration_label_sec.grid(row=1, column=2, sticky='we')
        self.expiration_year.grid(row=1, column=3, columnspan=1, sticky='we')
        self.security_code_label.grid(row=1, column=4, sticky='e')
        self.security_code.grid(row=1, column=5, columnspan=1, sticky='we')
        self.cardholder_label.grid(row=2, column=0, sticky='e')
        self.cardholder.grid(row=2, column=1, columnspan=5, sticky='we')

        self.columnconfigure(0, pad=4)
        self.columnconfigure(1, pad=4)
        self.columnconfigure(2, pad=4)
        self.columnconfigure(3, pad=4)
        self.columnconfigure(4, pad=4, weight=1)
        self.columnconfigure(5, pad=4)
        self.rowconfigure(0, pad=4)
        self.rowconfigure(1, pad=4)
        self.rowconfigure(2, pad=4)

        self.configure(padx=2, pady=2)

    def _on_frame_destroy(self):
        self.save_content()

        self.card_number.unbind("<FocusOut>")
        self.expiration_month.unbind("<FocusOut>")
        self.expiration_year.unbind("<FocusOut>")
        self.security_code.unbind("<FocusOut>")
        self.cardholder.unbind("<FocusOut>")
