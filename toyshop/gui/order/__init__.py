"""
GUI F3 order submodule.
"""
from .basket import BasketFrame
from .checkout import CheckoutFrame
from .address import AddressFrame
from .payment import PaymentFrame
from .summary import SummaryFrame

frames = [
    BasketFrame,
    CheckoutFrame,
    AddressFrame,
    PaymentFrame,
    SummaryFrame,
]
