"""
Address input module.
"""
import tkinter as tk
from tkinter import messagebox

from toyshop.controllers.order import OrderController
from toyshop.util import FrameBase, Observer, StorableObserver


class AddressFrame(FrameBase):
    def _init_frame(self):
        # ROW 0
        self.title = tk.Label(self, text='Address', font=self.root_frame.font_title)
        self.title.grid(column=0, row=0, columnspan=2, sticky='w', padx=4, pady=4)

        # ROW 1
        self.subtext = tk.Label(self,
            text='Please enter your delivery/billing address. All fields must be filled in.')
        self.subtext.grid(column=0, row=1, columnspan=2, sticky='w', padx=4, pady=4)

        # ROW 2
        self.address = AddressSubFrame(self, subject=OrderController().current_order.address)
        self.address.grid(column=0, row=2, columnspan=2, rowspan=1, sticky='')

        # ROW 3
        self.prev_button = tk.Button(self, text='Previous', command=self.goto_checkout)
        self.prev_button.grid(column=0, row=3, sticky='w', padx=2, pady=2)
        self.next_button = tk.Button(self, text='Next', command=self.goto_payment)
        self.next_button.grid(column=1, row=3, sticky='e', padx=2, pady=2)

        self.columnconfigure(1, weight=1)
        self.rowconfigure(2, weight=1)

        self.configure(padx=2, pady=2)

    def goto_checkout(self):
        self.root_frame.load_content('CheckoutFrame')

    def goto_payment(self):
        self.address.save_content()

        if self.address.subject.validate():
            self.root_frame.load_content('PaymentFrame')
        else:
            messagebox.showerror("Validation Error", "Please check your input and try again.")

    def _on_frame_destroy(self):
        self.address.frame_destroy()


class AddressSubFrame(StorableObserver, FrameBase):
    def _init_frame(self):
        self.variables = {
            'first_name': tk.StringVar(),
            'last_name': tk.StringVar(),
            'street': tk.StringVar(),
            'city': tk.StringVar(),
            'postcode': tk.StringVar(),
            'country': tk.StringVar(value='United Kingdom'),
        }

        self.first_name_label = tk.Label(self, text='First name: ')
        self.first_name = tk.Entry(self, textvariable=self.variables['first_name'])
        self.last_name_label = tk.Label(self, text='Last name: ')
        self.last_name = tk.Entry(self, textvariable=self.variables['last_name'])
        # self.apartment_label = tk.Label(self, text='Apartment: ')
        # self.apartment = tk.Entry(self, textvariable=self.apartment_var)
        self.street_number_label = tk.Label(self, text='Street, number: ')
        self.street_number = tk.Entry(self, textvariable=self.variables['street'])
        self.city_label = tk.Label(self, text='City: ')
        self.city = tk.Entry(self, textvariable=self.variables['city'])
        self.postcode_label = tk.Label(self, text='Postcode: ')
        self.postcode = tk.Entry(self, textvariable=self.variables['postcode'])
        # self.country_label = tk.Label(self, text='Country: ')
        # self.country = tk.Entry(self, textvariable=self.country_var)

        self.first_name.bind("<FocusOut>", lambda event: self.save_content('first_name'))
        self.last_name.bind("<FocusOut>", lambda event: self.save_content('last_name'))
        self.street_number.bind("<FocusOut>", lambda event: self.save_content('street'))
        self.city.bind("<FocusOut>", lambda event: self.save_content('city'))
        self.postcode.bind("<FocusOut>", lambda event: self.save_content('postcode'))

        self.first_name_label.grid(row=0, column=0, sticky='e')
        self.first_name.grid(row=0, column=1, sticky='we')
        self.last_name_label.grid(row=0, column=2, sticky='e')
        self.last_name.grid(row=0, column=3, sticky='we')
        # self.apartment_label.grid(row=1, column=0, sticky='e')
        # self.apartment.grid(row=1, column=1, columnspan=3, sticky='we')
        self.street_number_label.grid(row=2, column=0, sticky='e')
        self.street_number.grid(row=2, column=1, columnspan=3, sticky='we')
        self.city_label.grid(row=3, column=0, sticky='e')
        self.city.grid(row=3, column=1, sticky='we')
        self.postcode_label.grid(row=3, column=2, sticky='e')
        self.postcode.grid(row=3, column=3, sticky='we')
        # self.country_label.grid(row=4, column=0, sticky='e')
        # self.country.grid(row=4, column=1, columnspan=3, sticky='we')

        self.columnconfigure(0, pad=4)
        self.columnconfigure(1, pad=4)
        self.columnconfigure(2, pad=4)
        self.columnconfigure(3, pad=4)
        self.rowconfigure(0, pad=4)
        self.rowconfigure(1, pad=4)
        self.rowconfigure(2, pad=4)
        self.rowconfigure(3, pad=4)
        self.rowconfigure(4, pad=4)
        self.rowconfigure(5, pad=4)

        self.configure(padx=2, pady=2)

    def _on_frame_destroy(self):
        self.save_content()

        self.first_name.unbind("<FocusOut>")
        self.last_name.unbind("<FocusOut>")
        self.street_number.unbind("<FocusOut>")
        self.city.unbind("<FocusOut>")
        self.postcode.unbind("<FocusOut>")
