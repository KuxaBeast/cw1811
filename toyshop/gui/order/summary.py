"""
Summary module.
"""
import tkinter as tk
from tkinter import filedialog
from tkinter.scrolledtext import ScrolledText

from toyshop.controllers.order import OrderController
from toyshop.util import FrameBase, Observer


class SummaryFrame(Observer, FrameBase):
    def _init_frame(self):
        # ROW 0
        self.title = tk.Label(self, text='Summary', font=self.root_frame.font_title)
        self.title.grid(column=0, row=0, columnspan=2, sticky='w', padx=4, pady=4)

        # ROW 1
        self.subtext = tk.Label(self,
            text='Your order has been successfully registered and your receipt generated.')
        self.subtext.grid(column=0, row=1, columnspan=6, sticky='w', padx=4, pady=4)

        # ROW 2
        self.receipt = ScrolledText(self)
        self.receipt.grid(column=0, row=2, columnspan=6, sticky='nsew', padx=2, pady=2)
        self.receipt.bind("<FocusIn>", self._select_all)
        self.receipt.bind("<Control-a>", self._select_all)

        self.assign_subject(OrderController().last_order)

        # ROW 3
        self.clipboard_button = tk.Button(
            self, text='Copy to clipboard', command=self._copy_to_clipboard)
        self.clipboard_button.grid(column=0, row=3, sticky='e', padx=2, pady=2)

        self.save_button = tk.Button(self, text='Save receipt', command=self._save_receipt)
        self.save_button.grid(column=1, row=3, sticky='e', padx=2, pady=2)

        self.next_button = tk.Button(self, text='Finish', command=self.goto_home)
        self.next_button.grid(column=5, row=3, sticky='e', padx=2, pady=2)

        self.columnconfigure(2, weight=1)
        self.rowconfigure(2, weight=1)

        self.configure(padx=2, pady=2)

    def _select_all(self, event):
        self.receipt.tag_add('sel', '1.0', 'end')
        self.receipt.mark_set('insert', '1.0')
        self.receipt.see('insert')
        return 'break'

    def _copy_to_clipboard(self):
        self.root_win.clipboard_clear()
        self.root_win.clipboard_append(self.subject.get_receipt())
        return 'break'

    def _save_receipt(self):
        filename = filedialog.asksaveasfilename(
            parent=self,
            title='Select where to save the receipt',
            filetypes=[('Plaintext file', '*.txt')],
        )
        if filename:
            self.subject.save_receipt(filename)

    def update_content(self):
        receipt = self.subject.get_receipt()
        self.receipt['state'] = 'normal'
        self.receipt.delete('1.0', 'end')
        self.receipt.insert('1.0', receipt)
        self.receipt['state'] = 'disabled'

    def goto_home(self):
        pass
