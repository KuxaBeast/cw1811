"""
Basket GUI module.
"""
import tkinter as tk
from tkinter import messagebox

from toyshop.util import FrameBase, Observer, ScrollableFrameBase
from toyshop.config import CURRENCY
from toyshop.models.item import Item, ItemInOrder
from toyshop.controllers.order import OrderController

class BasketFrame(FrameBase):
    def _init_frame(self):
        self.total_var = tk.StringVar()

        basket = OrderController().current_basket

        # ROW 0
        self.title = tk.Label(self, text='Your basket', font=self.root_frame.font_title)
        self.title.grid(column=0, row=0, columnspan=2, sticky='w', padx=4, pady=4)

        self.total = tk.Label(self, textvariable=self.total_var)
        self.total.grid(column=2, row=0, columnspan=2, sticky='e', padx=2, pady=2)

        # ROW 1
        self.itemsframe = BasketItemsFrame(self, self.total_var, subject=basket)
        self.itemsframe.grid(column=0, row=1, columnspan=4, sticky='nsew')#, padx=2, pady=2)

        self.rowconfigure(1, weight=1)

        # ROW 2
        self.discount_comment = tk.Label(self, text='Discount code may be applied at checkout.')
        self.discount_comment.grid(column=0, row=2, sticky='w', padx=4, pady=4)

        def add_items():
            for item in Item.get_all():
                basket.add_item(item)
        self.test_button = tk.Button(self, text='Fill Basket', command=add_items)
        self.test_button.grid(column=2, row=2, sticky='e', padx=2, pady=2)

        self.checkout_button = tk.Button(self, text='Check Out', command=self.goto_checkout)
        self.checkout_button.grid(column=3, row=2, sticky='e', padx=2, pady=2)

        self.columnconfigure(2, weight=1)
        self.configure(padx=2, pady=2)

    def goto_checkout(self):
        if len(self.itemsframe.subject.items) > 0:
            self.root_frame.load_content('CheckoutFrame')
        else:
            messagebox.showerror(
                "Checkout Error",
                "To check out your order, please add at least one item to your basket."
            )

    def _on_frame_destroy(self):
        self.itemsframe.frame_destroy()


class BasketItemsFrame(Observer, ScrollableFrameBase):
    def _init_frame(self, total_var):
        self.total = total_var
        self._itemframes = []

        # 1px because contained items have also 1px, which makes 2px total
        self.scrl_frame.configure(padx=1, pady=1)

    def update_content(self):
        """Update the state of items in the basket GUI."""
        # Get a set of ItemInOrder instances from existing subframes
        current = set(map(lambda x: x.subject, self._itemframes))
        # In removed items order is not relevant
        removed = current-set(self.subject.items)
        # but when adding new, we want to make sure to get them in order
        added = [i for i in self.subject.items if i not in current]

        # Remove all old items
        for item in removed:
            self._remove_item(item)

        # Add all new items
        for item in added:
            self._add_item(item)

        # Update total variable
        self.total.set(f"Total price: {self.subject.total} {CURRENCY}")

    def _add_item(self, item):
        """Create new ItemFrame instance, places it and register it."""
        itemframe = BasketItemFrame(self.scrl_frame, subject=item)
        itemframe.pack(fill='x', padx=1, pady=1)
        self._itemframes.append(itemframe)

    def _remove_item(self, item):
        """Get an existing ItemFrame instance and unregister it."""
        itemframe = next(x for x in self._itemframes if x.subject is item)
        itemframe.forget()
        self._itemframes.remove(itemframe)

    def _on_frame_destroy(self):
        for itemframe in self._itemframes:
            itemframe.frame_destroy()


class BasketItemFrame(Observer, FrameBase):
    def _init_frame(self):
        self.id_var = tk.StringVar()
        self.name_var = tk.StringVar()
        self.price_var = tk.StringVar()

        self.amount_var = tk.IntVar()

        self.name_label = tk.Label(self, textvariable=self.name_var)
        self.price_label = tk.Label(self, anchor='w', textvariable=self.price_var)

        # SPINBOX INSTEAD?
        self.amount_entry = tk.Entry(self, textvariable=self.amount_var, width=4)
        self.remove_button = tk.Button(self, text='X', command=self.remove_self, width=2)

        self.amount_entry.bind("<Key-Return>", lambda event: self.save_amount())
        self.amount_entry.bind("<FocusOut>", lambda event: self.save_amount())
        self.amount_entry.bind("<FocusIn>", lambda e: e.widget.selection_range(0, 'end'))

        self.name_label.pack(side='left')
        self.remove_button.pack(side='right', padx=2, pady=2, ipadx=0, ipady=0)
        self.price_label.pack(side='right', padx=4)
        self.amount_entry.pack(side='right')

        self.configure(relief='ridge', borderwidth=2)

    def update_content(self):
        """Update item variables."""
        self.id_var.set(self.subject.item.id)
        self.name_var.set(self.subject.item.name)
        self.price_var.set(f"x {self.subject.price} {CURRENCY} = {self.subject.total()} {CURRENCY}")
        self.amount_var.set(self.subject.amount)

    def save_amount(self):
        """Apply item amount."""
        amount = self.subject.amount

        try:
            amount = self.amount_var.get()
        except:
            pass
        finally:
            if amount == 0:
                # Hack to remove item from basket if amount entered is zero
                self.remove_self()
            else:
                self.subject.amount = amount

    def remove_self(self, remove_all=True):
        """Remove itself from the basket."""
        self.parent.subject.remove_item(self.subject, remove_all=remove_all)

    def _on_frame_destroy(self):
        # self.save_amount()

        self.amount_entry.unbind("<Key-Return>")
        self.amount_entry.unbind("<FocusOut>")
        self.amount_entry.unbind("<FocusIn>")
