"""
Checkout module.
"""
import tkinter as tk
from tkinter.font import Font

from toyshop.config import CURRENCY
from toyshop.controllers.order import OrderController
from toyshop.models.order_misc import DeliveryType
from toyshop.util import FrameBase, Observer


class CheckoutFrame(Observer, FrameBase):
    def _init_frame(self):
        self.summary_var = tk.StringVar()
        self.promo_var = tk.StringVar()
        self.delivery_summary_var = tk.StringVar()

        order = OrderController().current_order
        self.assign_subject(order)

        # ROW 0
        self.title = tk.Label(self, text='Checkout', font=self.root_frame.font_title)
        self.title.grid(column=0, row=0, columnspan=2, sticky='w', padx=4, pady=4)

        # ROW 1
        self.delivery = DeliverySelectionSubFrame(self, subject=order)
        self.delivery.grid(column=0, row=1, columnspan=2, sticky='nsew')

        self.summary = tk.Label(
            self, textvariable=self.summary_var,
            justify='right', font=Font(self, ('sans-serif',10,'bold'))
        )
        self.summary.grid(column=3, row=1, columnspan=2, rowspan=2, sticky='ne', padx=4, pady=4)

        # ROW 2
        self.delivery_summary = tk.Label(
            self, textvariable=self.delivery_summary_var,
            justify='left', font=Font(self, ('sans-serif',9,'bold'))
        )
        self.delivery_summary.grid(column=0, row=2, columnspan=2, sticky='nw', padx=4, pady=4)

        # ROW 3
        self.promo_code = tk.Entry(self, textvariable=self.promo_var)
        self.promo_code.grid(column=0, row=3, sticky='we', padx=2, pady=2)
        self.promo_submit = tk.Button(self, text="Apply Promo Code")#, command=self._apply_promo)
        self.promo_submit.grid(column=1, row=3, sticky='w', padx=2, pady=2)

        self.promo_var.set('Discount feature not yet implemented.')
        self.promo_code.config(state='disabled')
        self.promo_submit.config(state='disabled')

        self.next_button = tk.Button(self, text='Next', command=self.goto_address)
        self.next_button.grid(column=4, row=3, sticky='e', padx=2, pady=2)

        self.columnconfigure(0, weight=1)
        self.columnconfigure(3, weight=1)
        # self.rowconfigure(1, weight=1)
        self.rowconfigure(2, weight=1)

        self.configure(padx=2, pady=2)

    def update_content(self):
        # Update summary
        basket, delivery, total = self.subject.checkout_summary
        self.summary_var.set(
            f"""Basket total: {basket} {CURRENCY}
Delivery: {delivery} {CURRENCY}
Total: {total} {CURRENCY}"""
        )

        delivery = self.subject.delivery
        self.delivery_summary_var.set(
            f"""{delivery.title} 
Price: {delivery.price} {CURRENCY}
{f"Takes {delivery.days_to_deliver} days to deliver." if DeliveryType(delivery.type) is not DeliveryType.COLLECTION else "Personal pick-up."}"""
        )

    # def _apply_promo(self):
    #     flag = self.subject.apply_discount(self.promo_var.get())
    #     if flag:
    #         self.promo_var.set("")
    #     # Propably some kind of error message?

    def goto_address(self):
        self.root_frame.load_content('AddressFrame')

    def _on_frame_destroy(self):
        self.delivery.frame_destroy()


class DeliverySelectionSubFrame(Observer, FrameBase):
    def _init_frame(self):
        self._current_options = []
        self._selection = {}
        self._common_var = tk.IntVar()
        self._common_var.trace_add('write', self._var_callback)
        self._prevent_callback = False

        self.columnconfigure(0, weight=1)

    def _var_callback(self, *_):
        if not self._prevent_callback:
            self.subject.delivery = self._common_var.get()
        self._prevent_callback = False

    def update_content(self):
        new_opts = self.subject.delivery_options

        # Update options if set changed
        if self._current_options != new_opts:
            # Remove all radio buttons
            for radio in self._selection.values():
                radio.grid_forget()
            self._selection.clear()

            self._current_options = new_opts
            for method in self._current_options:
                radio = tk.Radiobutton(
                    self,
                    text=method.title,
                    variable=self._common_var,
                    value=method.id
                )
                radio.grid(sticky='w', padx=1, pady=1)
                self._selection[method.id] = radio

        # Update selection value if no valid selected
        if self._common_var.get() not in self._selection.keys():
            self._prevent_callback = True
            self._common_var.set(self.subject.delivery.id)
