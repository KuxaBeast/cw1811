"""
Core main GUI window definition.
"""
import tkinter as tk
from tkinter.font import Font

from toyshop.util import FrameBase
from toyshop.config import WIN_NAME

# The main base window is defined here
# it's built so you can switch content of the main window in runtime
class MainWindow(FrameBase):
    """Main base window class.

    Allows to easily swap content frames with its `load_content` method."""
    def __init__(self, *args, **kwargs):
        super().__init__(tk.Tk(), *args, **kwargs)
        self.pack(fill='both', expand=1)

    def _init_frame(self, default_load=None, **kwargs):
        """Master window/frame initialization logic."""
        self._default_load = default_load
        self._loaded = None
        self._content_widgets = {}

        self.root_win.title(WIN_NAME)
        self.root_win.minsize(600, 300)

        # Titles font
        self.font_title = Font(self, font=('sans-serif',12,'bold'))

        self.header_bar = HeaderFrame(self)
        self.header_bar.grid(row=0, column=0, sticky='nsew')

        self.rowconfigure(0, minsize=10)
        self.rowconfigure(1, weight=1)
        self.columnconfigure(0, weight=1)

    def load_content(self, frame_class):
        """
        This function loads the main window frame. It selects
        from the `content_widgets` dictionary initialized in the `_init_frame` method.
        """
        if not isinstance(frame_class, str):
            frame_class = frame_class.__name__

        if frame_class not in self._content_widgets:
            raise Exception('This class does not have a registered content widget')

        if self._loaded is not None:
            # Prevent from reloading the same frame
            if type(self._loaded).__name__ == frame_class:
                return

            # Destroy currently loaded frame
            self._loaded.frame_destroy()

            # Remove the frame from view
            self._loaded.grid_forget()
        # del self._loaded

        # Replace by an instance of a new frame
        self._loaded = self._content_widgets[frame_class](self)
        self._loaded.grid(row=1, column=0, sticky='nsew', padx=0, pady=0)

    def register(self, content):
        """Register content frame for later use."""
        self._content_widgets.update({content.__name__: content})

    def show(self):
        """Entrypoint for GUI and window startup logic."""
        if self._default_load:
            self.load_content(self._default_load)

        self.mainloop()

class HeaderFrame(FrameBase):
    """A headerbar for easier navigation."""
    def _init_frame(self):
        self.homebtn = tk.Button(self, text='Home')
        # self.secndbtn = tk.Button(self, text='Second')
        self.loginbtn = tk.Button(
            self,
            text='Log In',
            command=lambda: self.master.load_content('LoginFrame')
        )
        self.basketbtn = tk.Button(
            self,
            text='Basket',
            command=lambda: self.master.load_content('BasketFrame')
        )

        self.homebtn.pack(side='left', fill='y')
        # self.secndbtn.pack(side='left', fill='y')

        self.basketbtn.pack(side='right', fill='y')
        self.loginbtn.pack(side='right', fill='y')
