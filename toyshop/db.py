"""
Module implementing database functionality
"""
from abc import ABCMeta, abstractmethod
import sqlite3
from contextlib import contextmanager
from typing import Dict, List, Union

from toyshop.config import DATABASE
from toyshop.util import ObservedObject

def db_factory():
    """Factory function returning a database connection instance."""
    return sqlite3.connect(DATABASE)

# Database connection creation
conn = db_factory()

@contextmanager
def cursor():
    """A cursor transaction generator for use with `with` statement."""
    cur = conn.cursor()
    try:
        yield cur
    except:
        conn.rollback()
        raise
    else:
        conn.commit()
    finally:
        cur.close()

class ModelBase(metaclass=ABCMeta):
    """Base abstract class for model definition."""
    def __init__(self, *args, **kwargs):
        # assign positional arguments
        args = iter(args)
        for col in self.attributes:
            setattr(self, '_'+col, next(args, None))

        # assign keyword arguments
        for key,arg in kwargs.items():
            if key in self.attributes:
                setattr(self, '_'+key, arg)

    def __getattr__(self, name):
        if name in self.attributes:
            return getattr(self, '_'+name)
        raise AttributeError(f"'{type(self).__name__}' object has no attribute '{name}'")

    def __setattr__(self, name, value):
        if name in self.attributes:
            super().__setattr__('_'+name, value)

            if isinstance(self, ObservedObject):
                self._notify()
            return

        super().__setattr__(name, value)

    @property
    @abstractmethod
    def attributes(self):
        """Used attributes of current model. Should not contain the id column."""
        raise NotImplementedError

    def as_dict(self, attributes=None):
        """Return dict of attributes and their values."""
        return {
            x:getattr(self, x) for x in self.attributes
            if attributes is None or x in attributes
        }

    def validate(self):
        """Basic model validation method."""
        # check if all attributes are assigned
        attrs_exist = [
            getattr(self, attr) not in (None, "")
            for attr
            in self.attributes
        ]
        if not all(attrs_exist):
            return False

        # check for custom validation methods and invoke them
        validate_methods = [
            (attr, getattr(self, f'{attr}_validate'))
            for attr in self.attributes
            if hasattr(self, f'{attr}_validate')
        ]
        if not all(fun(getattr(self, attr)) for attr,fun in validate_methods if callable(fun)):
            return False

        return True


class DbBase(ModelBase, metaclass=ABCMeta):
    """Base abstract class for model definition with DB functionality."""
    def __init__(self, *args, **kwargs):
        if self.id_column is not None:
            setattr(self, self.id_column, kwargs.pop(self.id_column, None))
        super().__init__(*args, **kwargs)

    id_column = 'id'

    @property
    def id_value(self):
        """Name of the identification column."""
        return getattr(self, self.id_column, None)

    def __eq__(self, other):
        if isinstance(other, type(self)):
            return hash(self) == hash(other)
        return False

    # Fix for 'unhashable type' exception when sorting in `set`
    def __hash__(self):
        # if id column assigned, return hash of id value
        if self.id_column is not None and self.id_value is not None:
            return hash(self.id_value)

        # if multiple private keys and all set, hash of them
        if isinstance(self, MultiPkDbBase) and self.all_ids_set():
            return hash(tuple(self.id_value_dict.values()))

        return super().__hash__()

    @property
    @abstractmethod
    def schema(self):
        """Schema of the current model."""
        raise NotImplementedError

    @property
    @abstractmethod
    def model_name(self):
        """Table name for the current model."""
        raise NotImplementedError

    def push(self, insert=None):
        """
        Push model into database.
        If new, model gets inserted and `id` column generated.
        If `id` column specified, database record is updated in place.

        - insert:
            None - let program decide
            True - force insert
            False - force update
        """
        if isinstance(self, MultiPkDbBase) and not self.all_ids_set():
            raise AttributeError("Not all ids are set.")

        # If insert is not forced, decide using id value
        if insert is None and self.id_column is not None:
            # False if id not set, True if id set
            insert = self.id_value is None

        rowcount = 0
        # Force update on False or update on None by default
        if insert is not True:
            rowcount = self._update()

        # Force insert on True or insert if update didn't succeed and was not forced
        if insert or (insert is None and rowcount == 0):
            rowcount = self._insert()

        return rowcount

    def _insert(self):
        """Internal database insert method."""
        with cursor() as cur:
            sql = "INSERT INTO `{0}` (`{1}`) VALUES ({2});".format(
                self.model_name,
                '`,`'.join(self.attributes),
                self._sql_multi_values(self.attributes)
            )

            data = self.as_dict()
            rowcount = cur.execute(sql, data).rowcount

            # get newly generated id from database
            if self.id_column is not None:
                cur.execute(
                    f"SELECT `{self.id_column}` FROM `{self.model_name}` WHERE _rowid_ = ?",
                    (cur.lastrowid,)
                )
                setattr(self, self.id_column, cur.fetchone()[0])

            return rowcount

    def _update(self):
        """Internal database update method."""
        with cursor() as cur:
            sql = "UPDATE `{0}` SET {1} WHERE {2};".format(
                self.model_name,
                self._sql_multi_set(self.attributes),
                self._sql_multi_where([self.id_column] if self.id_column else self.id_columns)
            )

            data = self.as_dict()
            if self.id_column is not None:
                data.update({self.id_column:getattr(self, self.id_column, None)})

            return cur.execute(sql, data).rowcount

    def remove(self):
        """Remove current model from table."""
        if isinstance(self, MultiPkDbBase) and not self.all_ids_set():
            raise AttributeError("Not all ids are set.")

        if self.id_column and self.id_value is None:
            raise Exception('Cannot remove model. ID is not defined.')

        with cursor() as cur:
            sql = "DELETE FROM `{0}` WHERE {1}".format(
                self.model_name,
                self._sql_multi_where([self.id_column] if self.id_column else self.id_columns)
            )
            data = (self.id_value,) if self.id_column else self.id_value_dict

            return cur.execute(sql, data).rowcount

    @classmethod
    def get_by_id(cls, id_val: int):
        """Retrieve record by the id column and create a new model object."""
        return cls.get_one_where(**{cls.id_column:id_val})

    @classmethod
    def get_one_where(cls, **kwargs):
        """Retrieve one record by values in selected columns."""
        try:
            return next(cls.get_where(**kwargs))
        except StopIteration:
            return None

    @classmethod
    def get_where(cls, **kwargs):
        """Retrieve all records by values in selected columns."""
        cols = cls.attributes+[cls.id_column] if cls.id_column else cls.attributes
        with cursor() as cur:
            sql = "SELECT `{1}` FROM `{0}`{2}".format(
                cls.model_name,
                '`,`'.join(cols),
                " WHERE "+cls._sql_multi_where(list(kwargs.keys())) if kwargs else ""
            )

            cur.execute(sql, kwargs)
            for row in cur.fetchall():
                yield cls(**dict(zip(cols, row)))

    @classmethod
    def get_all(cls):
        """Retrieve all records from table and return as model objects."""
        return cls.get_where()

    @staticmethod
    def _sql_multi_where(columns):
        """Helper function for generating SQL queries' WHERE specifier sequences."""
        return " AND ".join("`{0}`=:{0}".format(col) for col in columns)

    @staticmethod
    def _sql_multi_set(columns):
        """Helper function for generating SQL queries' SET specifier sequences."""
        return ", ".join("`{0}`=:{0}".format(x) for x in columns)

    @staticmethod
    def _sql_multi_values(columns):
        """Helper function for generating SQL queries' value sequences."""
        return ", ".join(f":{x}" for x in columns)


class MultiPkDbBase(DbBase, metaclass=ABCMeta):
    """Multi-private key implementation of `DbBase`. Not elegant but serves its purpose

    `id_column` must be set to `None` and `id_columns` should be a list of PK attributes."""
    id_column = None

    @property
    @abstractmethod
    def id_columns(self):
        """Tuple of names of the identification attributes."""
        raise NotImplementedError

    @property
    def id_value_dict(self):
        """Dict of id column attribute values."""
        if set(self.id_columns).intersection(self.attributes) == set(self.attributes):
            raise AttributeError("Ids do not exist in used attributes.")
        return {attr:getattr(self, attr, None) for attr in self.id_columns}

    def all_ids_set(self):
        """Check if all id attributes set."""
        return all(self.id_value_dict.values())
