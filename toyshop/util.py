"""
Utility module containing abstract classes.
"""
import tkinter as tk
import traceback
from abc import ABCMeta, abstractmethod


class FrameBase(tk.Frame, metaclass=ABCMeta):
    """An abstract class to define Tkinter Frames."""
    def __init__(self, master=None, *args, **kwargs):
        self.master = master
        super().__init__(master=self.master)

        self._init_frame(*args, **kwargs)

    @abstractmethod
    def _init_frame(self, *args, **kwargs):
        """Definition of frame behaviour and appearance. Is called from the default constructor."""
        raise NotImplementedError('The _init_frame method was not implemented.')

    @property
    def root_frame(self):
        """Return the root frame."""
        return self if isinstance(self.master, tk.Tk) else self.parent.root_frame

    @property
    def root_win(self):
        """Return the root window element."""
        return self.root_frame.master

    @property
    def parent(self):
        """Return last `FrameBase` instance in hierarchy."""
        obj = self.master
        while not isinstance(obj, FrameBase):
            obj = getattr(obj, 'master')
        return obj

    def frame_destroy(self):
        """
        Call this to gracefully destroy subframe.

        By calling this on an `Observer` instance, you dereference the observer from the subject
        and disable updating of the not-anymore existing frame.
        """
        # safely destroy children first
        self._on_frame_destroy()
        # then deassign subjeft and destroy self
        if isinstance(self, Observer):
            self.deassign_subject()

    def _on_frame_destroy(self):
        """Override to implement behaviour on the call of the `frame_destroy` method.
        Destroy child frames here."""


class ScrollableFrame(tk.Frame):
    """Scrollable frame class."""
    def __init__(self, master, *args, **kwargs):
        super().__init__(master=master, *args, **kwargs)

        self._canv = tk.Canvas(self)
        self._canv.pack(side="left", fill="both", expand=True)

        scrollbar = tk.Scrollbar(self, orient="vertical", command=self._canv.yview)
        scrollbar.pack(side="right", fill="y")

        self._canv.configure(yscrollcommand=scrollbar.set)
        self._canv.bind("<Configure>", self._cv_config)

        self._canv.bind_all("<MouseWheel>", self._on_mousewheel) # For Windows
        self._canv.bind_all("<Button-4>", self._on_mousewheel) # For Linux
        self._canv.bind_all("<Button-5>", self._on_mousewheel) # For Linux

        self.scrl_frame = tk.Frame(self._canv)
        self._frame_id = self._canv.create_window((0, 0), window=self.scrl_frame, anchor="nw")

    def _cv_config(self, e):
        """Configure callback on resize."""
        # get requested widget sizes
        width, height = self.scrl_frame.winfo_reqwidth(), self.scrl_frame.winfo_reqheight()
        # configure child frame and canvas
        self._canv.itemconfig(
            self._frame_id, width=e.width,
            height=e.height-2 if e.height-2>height else height
            # sizes adjusted to fix misbehaviour (still weird on windows)
        )
        self._canv.configure(scrollregion=self._canv.bbox("all"))

    def _on_mousewheel(self, e):
        """Mousewheel callback."""
        yscroll = int(-1*(e.delta/120))
        if yscroll == 0:
            if e.num == 4:
                yscroll = -1
            elif e.num == 5:
                yscroll = 1

        self._canv.yview_scroll(yscroll, "units")

class ScrollableFrameBase(FrameBase, ScrollableFrame, metaclass=ABCMeta):
    """Convenience `FrameBase` class implementing `ScrollableFrame`."""


class Observer(metaclass=ABCMeta):
    """
    An abstract class to define updatable frame entities.
    Based on Observer pattern. One has to implement the `update_content` method
    which is called whenever there is an update to the associated `subject`.

    Any observers have to be attached to subjects to actually react to changes.
    Can be used without any subjects, being updated manually.

    Assigns the subject and calls the `update_content` method after initialization.

    If not supplied to the constructor, it HAS to be assigned in the init sequence.
    Otherwise expect undocumented behaviour.
    """
    def __init__(self, *args, subject=None, **kwargs):
        self._subject = None

        super().__init__(*args, **kwargs)
        if subject is not None:
            self.assign_subject(subject)

    @abstractmethod
    def update_content(self):
        """Update content with data from the associated object."""
        raise NotImplementedError

    @property
    def subject(self):
        """The associated object."""
        return self._subject

    def assign_subject(self, subject):
        """Assign a new subject."""
        self.deassign_subject()

        self._subject = subject

        # Attach if observable
        if isinstance(subject, ObservedObject):
            self._subject.attach(self)

        self.update_content()

    def deassign_subject(self):
        """Detach if assigned and observable, then set to None."""
        if isinstance(self._subject, ObservedObject):
            self._subject.detach(self)
        self._subject = None


class StorableObserver(Observer):
    """
    This class allows implements universal interface to update models
    by changes in defined variables and being updated by models.

    To take advantage of this class, override `variables` dictionary
    with model attribute names and variables, which are to be synchronized,
    and on update of the specific variable invoke `save_content` method,
    which will update associated model. The `update_content` method from the `Observer` class
    is overriden to update GUI.
    """
    variables = {}

    def update_content(self):
        for attr,var in self.variables.items():
            val = getattr(self.subject, attr)
            if val is not None:
                var.set(val)

    def save_content(self, attrs_filter=None):
        """
        Save variable values to models.

        Affected variables can be specified with the `attrs_filter` argument.
        """
        for attr,var in self.variables.items():
            if attrs_filter and attr not in attrs_filter:
                continue

            if attr not in self._subject.attributes:
                raise KeyError

            val = getattr(self._subject, attr)
            try:
                val = var.get()
            except:
                pass
            finally:
                setattr(self._subject, attr, val)


class ObservedObject(metaclass=ABCMeta):
    """
    Helper class implementing an interface of subject side of an Observer pattern.

    Allows for "observer" objects to subscribe to notifications
    for changes on children of this class.
    """
    _observers = set()

    def attach(self, observer):
        """Attach an observer to send notifications to later."""
        self._observers.add(observer)

    def detach(self, observer):
        """Detach an observer."""
        self._observers.remove(observer)

    def _notify(self):
        """Notify all registered observers for change."""
        # create immutable copy so that it doesn't throw exception on change
        for observer in tuple(self._observers):
            try:
                observer.update_content()
            except:
                traceback.print_exc()

    def deassign_all(self):
        for observer in self._observers:
            try:
                observer.deassign_subject()
            except:
                traceback.print_exc()

class Singleton(type):
    """Singleton metaclass implementation.

    Implementation source used from:
    https://stackoverflow.com/questions/6760685/creating-a-singleton-in-python"""
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]
